import React from "react";

interface State {
  devices: MediaDeviceInfo[];
  show: boolean;
}

async function enumerateDevices() {
  if (
    "mediaDevices" in navigator &&
    typeof navigator.mediaDevices.enumerateDevices === "function"
  ) {
    return navigator.mediaDevices.enumerateDevices();
  }
  throw new Error("enumerateDevices is not implemented in this browser");
}

export class CameraSwitcher extends React.PureComponent<any, State> {
  private selector: React.RefObject<HTMLSelectElement>;

  constructor(p: any) {
    super(p);
    this.state = {
      devices: [],
      show: false,
    };
    this.selector = React.createRef();
  }

  onClick = async () => {
    try {
      this.setState({
        devices: await enumerateDevices(),
        show: true,
      });
    } catch (e) {
      alert(e);
    }
  };

  onChange = (ev: any) => {
    this.cameraId = ev.target.value;
    alert("請重新載入掃描頁面");
    window.history.back();
  };

  get cameraId() {
    return window.localStorage?.getItem("cameraId") || "";
  }

  set cameraId(v) {
    v
      ? window.localStorage?.setItem("cameraId", v)
      : window.localStorage?.removeItem("cameraId");
  }

  render() {
    const { children } = this.props;
    const { show, devices } = this.state;
    return (
      <div onClick={this.onClick}>
        {children}
        {show && (
          <>
            <br />
            <select onChange={this.onChange} value={this.cameraId}>
              <option value="">自動</option>
              {devices
                .filter((x) => x.kind === "videoinput")
                .map((x) => (
                  <option value={x.deviceId} key={x.deviceId}>
                    {x.label}
                  </option>
                ))}
            </select>
          </>
        )}
      </div>
    );
  }
}
